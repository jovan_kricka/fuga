# Space Invaders Detector

[![N|Solid](http://www.pngmart.com/files/4/Space-Invaders-PNG-HD.png)](http://www.pngmart.com/files/4/Space-Invaders-PNG-HD.png)

# Requirements

  - Space Invaders Detector requires Maven version 3.5.2
  - It uses Project Lombok for boilerplate code so Lombok plugin is needed for IntelliJ
  - It is built on top of Java 8

# Instructions

Space Invaders Detector is a console application that gives you possibility to detect space invaders from pre-defined set in your radar images.

  - run *mvn clean install* to build the project
  - run *java -jar target\spaceinvaders-x.x.x-SNAPSHOT.jar* to start the app
  - drop radar images to the root directory of the project and observe the console output

Happy space invader detection!