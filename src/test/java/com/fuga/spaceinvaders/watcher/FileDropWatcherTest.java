package com.fuga.spaceinvaders.watcher;

import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;

public class FileDropWatcherTest {

    private static final String FILE_PATH = "simple_radar_image" + UUID.randomUUID().toString() + ".txt";

    private final AtomicInteger linesProcessed = new AtomicInteger(0);

    @Test
    public void run_textFileGiven_textFileSuccessfullyProcessed() throws InterruptedException, IOException {

        final FileDropWatcher fileDropWatcher = getFileDropWatcher("./");

        final Thread fileDropWatcherThread = new Thread(fileDropWatcher);
        fileDropWatcherThread.start();

        Thread.sleep(30);

        final List<String> fileContent = new ArrayList<>();
        fileContent.add("---oo---");
        fileContent.add("-o-oo-o-");
        writeLinesToFile(fileContent, FILE_PATH);
        Thread.sleep(30);

        fileDropWatcherThread.interrupt();
        fileDropWatcherThread.join();

        assertEquals(2, linesProcessed.get());
        deleteFile(FILE_PATH);

    }

    @Test(expected = IOException.class)
    public void run_badDirectoryGiven_fileDropWatcherFinished() throws IOException {
        getFileDropWatcher("./i_do_not_really_exist");
    }

    private FileDropWatcher getFileDropWatcher(final String directoryPath) throws IOException {

        return new FileDropWatcher(directoryPath, lines -> {

            for (final String line : lines) {
                linesProcessed.incrementAndGet();
            }

        });

    }

    private void writeLinesToFile(final List<String> lines, final String path) throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter(path));

        for (final String line : lines) {

            writer.write(line);
            writer.newLine();

        }

        writer.close();

    }

    private void deleteFile(final String path) {

        final File file = new File(path);
        file.delete();

    }
}
