package com.fuga.spaceinvaders.runner;

import com.fuga.spaceinvaders.service.UserInputService;
import com.fuga.spaceinvaders.watcher.FileDropWatcher;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class ImageDetectionRunnerTest {

    private UserInputService userInputService = mock(UserInputService.class);
    private FileDropWatcher fileDropWatcher = mock(FileDropWatcher.class);

    private final ImageDetectionRunner imageDetectionRunner = spy(new ImageDetectionRunner(
            userInputService,
            fileDropWatcher
    ));

    @Test
    public void runRadarImageDetection_fileDropWatcherMocked_threadsAndExecutorServiceFinished() {

        doNothing().when(fileDropWatcher).run();
        doNothing().when(userInputService).waitForExitSignal();

        imageDetectionRunner.runRadarImageDetection();

    }

}
