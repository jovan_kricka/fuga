package com.fuga.spaceinvaders.transformer;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FileContentToMatrixTransformerTest {

    private final FileContentToMatrixTransformer transformer = new FileContentToMatrixTransformer();

    @Test
    public void apply_fileContentGiven_correctMatrixReturned() {

        final List<String> fileContent = new ArrayList<>();
        fileContent.add("---oo---");
        fileContent.add("-o-oo-o-");

        final char[][] matrix = transformer.apply(fileContent);

        for (int i = 0; i < fileContent.size(); i++) {

            for (int j = 0; j < fileContent.get(i).length(); j++) {
                assertEquals(fileContent.get(i).charAt(j), matrix[i][j]);
            }

        }

    }

    @Test
    public void apply_emptyFileContentGiven_emptyMatrixReturned() {

        final char[][] matrix = transformer.apply(Collections.emptyList());

        assertEquals(0, matrix.length);

    }

}
