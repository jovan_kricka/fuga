package com.fuga.spaceinvaders.service;

import com.fuga.spaceinvaders.model.Detection;
import org.junit.Test;

import java.util.List;

import static com.fuga.spaceinvaders.service.SpaceInvaderDetectionService.KNOWN_SPACE_INVADERS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SpaceInvaderDetectionServiceTest {

    private final SpaceInvaderDetectionService detectionService = new SpaceInvaderDetectionService();

    @Test
    public void apply_emptyRadarImageGiven_noDetectionsReturned() {

        final char[][] radarImage = new char[0][];

        final List<Detection> detections = detectionService.apply(radarImage);

        assertTrue(detections.isEmpty());

    }

    @Test
    public void apply_radarImageWithNoSpaceInvadersGiven_noDetectionsReturned() {

        final char[][] radarImage = {
                {'-', '-', 'o', 'o', '-', '-'},
                {'-', '-', '-', '-', '-', '-'},
                {'-', '-', 'o', 'o', '-', '-'}
        };

        final List<Detection> detections = detectionService.apply(radarImage);

        assertTrue(detections.isEmpty());

    }

    @Test
    public void apply_radarImageWithSpaceInvaderGiven_detectionOnCorrectPositionReturned() {

        final char[][] radarImage = {
                {'-', '-', 'o', 'o', '-', '-'},
                {'-', '-', 'o', 'o', '-', '-'},
                {'-', '-', '-', '-', '-', '-'}
        };

        final List<Detection> detections = detectionService.apply(radarImage);

        assertEquals(1, detections.size());
        assertEquals(0, detections.get(0).getX());
        assertEquals(2, detections.get(0).getY());
        assertEquals(KNOWN_SPACE_INVADERS.get(2), detections.get(0).getSpaceInvader());

    }

}
