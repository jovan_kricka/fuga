package com.fuga.spaceinvaders.service;

import com.fuga.spaceinvaders.transformer.FileContentToMatrixTransformer;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class RadarImageProcessorTest {

    private final FileContentToMatrixTransformer transformer = mock(FileContentToMatrixTransformer.class);
    private final SpaceInvaderDetectionService detectionService = mock(SpaceInvaderDetectionService.class);

    private final RadarImageProcessor radarImageProcessor = new RadarImageProcessor(transformer, detectionService);

    @Test
    public void process_linesGiven_linesTransformedAndDetectionDone() {

        final List<String> fileContent = new ArrayList<>();
        fileContent.add("--oo--");
        fileContent.add("-o-oo-");
        final char[][] radarImage = {
                {'-', '-', 'o', 'o', '-', '-'},
                {'-', 'o', '-', 'o', 'o', '-'}
        };
        doReturn(radarImage).when(transformer).apply(eq(fileContent));
        doReturn(Collections.emptyList()).when(detectionService).apply(eq(radarImage));


        radarImageProcessor.process(fileContent);

        verify(transformer, times(1)).apply(eq(fileContent));
        verify(detectionService, times(1)).apply(eq(radarImage));

    }

}
