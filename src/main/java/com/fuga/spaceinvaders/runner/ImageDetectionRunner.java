package com.fuga.spaceinvaders.runner;

import com.fuga.spaceinvaders.service.UserInputService;
import com.fuga.spaceinvaders.watcher.FileDropWatcher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Runner responsible to start file listener and user input scanner.
 */
@Slf4j
@Component
public class ImageDetectionRunner {

    private final ExecutorService executorService;
    private final UserInputService userInputService;
    private final FileDropWatcher fileDropWatcher;

    @Autowired
    ImageDetectionRunner(
            final UserInputService userInputService,
            final FileDropWatcher fileDropWatcher) {

        this.userInputService = userInputService;
        this.fileDropWatcher = fileDropWatcher;
        this.executorService = Executors.newSingleThreadExecutor();

    }

    /**
     * Starts new worker thread that starts listening for radar images and waits for user input.
     */
    @PostConstruct
    void runRadarImageDetection() {

        new Thread(() -> {

            log.info("Starting radar image detection service...");

            final Future listeningTask = startListeningForRadarImages();
            log.info("Started file drop listener...");

            userInputService.waitForExitSignal();
            listeningTask.cancel(true);

            log.info("Shutting down executor service...");
            executorService.shutdown();

        }).start();

    }

    /**
     * Submits {@link FileDropWatcher} to the {@link ExecutorService} to listen for new files in path
     * defined by the configuration.
     * When new file is available it is being processed.
     *
     * @return {@link Future} representing the task that listens for new files (this task is cancelable)
     */
    private Future startListeningForRadarImages() {
        return executorService.submit(fileDropWatcher);
    }

}
