package com.fuga.spaceinvaders.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Scanner;

/**
 * Responsible for waiting until user submits terminal string.
 */
@Slf4j
@Component
public class UserInputService {

    private final Scanner scanner;
    private final String terminalString;

    public UserInputService(@Value("${app.terminalString}") final String terminalString) {

        this.terminalString = terminalString;
        this.scanner = new Scanner(System.in);

    }

    /**
     * Waits until user submits terminal string to the input console. Note that this is a blocking method.
     */
    public void waitForExitSignal() {

        log.info("Type '" + terminalString + "' if you want to stop the service...");
        String userInput = scanner.nextLine();

        while (!terminalString.equals(userInput)) {
            userInput = scanner.nextLine();
        }

        log.info("Terminal signal received...");

    }

}
