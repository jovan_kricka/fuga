package com.fuga.spaceinvaders.service;

import com.fuga.spaceinvaders.model.Detection;
import com.fuga.spaceinvaders.model.SpaceInvader;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

/**
 * Responsible for detecting pre-known space invaders in given radar image.
 */
@Component
public class SpaceInvaderDetectionService implements Function<char[][], List<Detection>> {

    static final List<SpaceInvader> KNOWN_SPACE_INVADERS = new ArrayList<>();

    static {

        final char[][] spaceInvader1 = {
                {'-', '-', 'o', '-', '-', '-', '-', '-', 'o', '-', '-'},
                {'-', '-', '-', 'o', '-', '-', '-', 'o', '-', '-', '-'},
                {'-', '-', 'o', 'o', 'o', 'o', 'o', 'o', 'o', '-', '-'},
                {'-', 'o', 'o', '-', 'o', 'o', 'o', '-', 'o', 'o', '-'},
                {'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'},
                {'o', '-', 'o', 'o', 'o', 'o', 'o', 'o', 'o', '-', 'o'},
                {'o', '-', 'o', '-', '-', '-', '-', '-', 'o', '-', 'o'},
                {'-', '-', '-', 'o', 'o', '-', 'o', 'o', '-', '-', '-'},
        };
        KNOWN_SPACE_INVADERS.add(SpaceInvader.builder()
                .content(spaceInvader1)
                .build());
        final char[][] spaceInvader2 = {
                {'-', '-', '-', 'o', 'o', '-', '-', '-'},
                {'-', '-', 'o', 'o', 'o', 'o', '-', '-'},
                {'-', 'o', 'o', 'o', 'o', 'o', 'o', '-'},
                {'o', 'o', '-', 'o', 'o', '-', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'},
                {'-', '-', 'o', '-', '-', 'o', '-', '-'},
                {'-', 'o', '-', 'o', 'o', '-', 'o', '-'},
                {'o', '-', 'o', '-', '-', 'o', '-', 'o'},
        };
        KNOWN_SPACE_INVADERS.add(SpaceInvader.builder()
                .content(spaceInvader2)
                .build());
        final char[][] spaceInvader3 = {
                {'o', 'o'},
                {'o', 'o'}
        };
        KNOWN_SPACE_INVADERS.add(SpaceInvader.builder()
                .content(spaceInvader3)
                .build());

    }

    /**
     * Takes given matrix of characters representing radar image and finds every occurrence of known space invaders in
     * that image.
     *
     * @param radarImage matrix of characters representing radar image
     * @return list of {@link Detection}s
     */
    @Override
    public List<Detection> apply(final char[][] radarImage) {

        if (radarImage.length == 0) {
            return Collections.emptyList();
        }

        final List<Detection> detections = new ArrayList<>();

        for (int i = 0; i < radarImage.length; i++) {

            for (int j = 0; j < radarImage[i].length; j++) {
                detections.addAll(checkForSpaceInvaders(radarImage, i, j));
            }

        }

        return detections;

    }

    /**
     * Checks if any of pre-known space invaders are detected in given position of given radar image.
     *
     * @param radarImage matrix of characters representing radar image
     * @param x          row
     * @param y          column
     * @return list of {@link Detection}s
     */
    private List<Detection> checkForSpaceInvaders(final char[][] radarImage, final int x, final int y) {

        final List<Detection> detections = new ArrayList<>();

        for (final SpaceInvader spaceInvader : KNOWN_SPACE_INVADERS) {

            if (spaceInvader.getContent()[0][0] != radarImage[x][y]) {
                continue;
            }

            boolean spaceInvaderIsMatching = true;

            for (int i = 0; i < spaceInvader.getContent().length; i++) {

                for (int j = 0; j < spaceInvader.getContent()[i].length; j++) {

                    if (x + i >= radarImage.length ||
                            y + j >= radarImage[x + i].length ||
                            spaceInvader.getContent()[i][j] != radarImage[x + i][y + j]) {

                        spaceInvaderIsMatching = false;
                        break;

                    }

                }

                if (!spaceInvaderIsMatching) {
                    break;
                }

            }

            if (spaceInvaderIsMatching) {
                detections.add(Detection.builder().x(x).y(y).spaceInvader(spaceInvader).build());
            }

        }

        return detections;

    }
}
