package com.fuga.spaceinvaders.service;

import com.fuga.spaceinvaders.model.Detection;
import com.fuga.spaceinvaders.transformer.FileContentToMatrixTransformer;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Responsible for processing radar image, detecting space invaders and printing it on output.
 */
@Slf4j
@AllArgsConstructor
@Component
public class RadarImageProcessor implements LinesProcessor {

    private final FileContentToMatrixTransformer transformer;
    private final SpaceInvaderDetectionService detectionService;

    /**
     * Transforms given file content lines to the radar image, detects space invaders on
     * that image and prints out detections.
     *
     * @param lines list of strings representing image file lines
     */
    @Override
    public void process(final List<String> lines) {

        final char[][] radarImage = transformer.apply(lines);
        final List<Detection> detections = detectionService.apply(radarImage);
        printDetections(detections);

    }

    /**
     * Prints out given detections to the log.
     *
     * @param detections list of {@link Detection}
     */
    private void printDetections(final List<Detection> detections) {

        if (CollectionUtils.isEmpty(detections)) {
            log.info("No space invaders detected...");
            return;
        }

        log.info("Following space invaders were detected: " + detections);

    }
}
