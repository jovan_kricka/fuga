package com.fuga.spaceinvaders.service;

import java.util.List;

/**
 * Responsible for processing lines of text.
 */
public interface LinesProcessor {

    void process(List<String> lines);

}
