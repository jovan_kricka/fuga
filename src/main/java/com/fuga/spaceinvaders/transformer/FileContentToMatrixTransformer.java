package com.fuga.spaceinvaders.transformer;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.function.Function;

/**
 * Responsible for converting given list of string to matrix of characters.
 */
@Component
public class FileContentToMatrixTransformer implements Function<List<String>, char[][]> {

    /**
     * Converts list of strings to matrix of characters.
     *
     * @param lines list of strings
     * @return two dimensional array of characters
     */
    @Override
    public char[][] apply(final List<String> lines) {

        if (CollectionUtils.isEmpty(lines)) {
            return new char[0][];
        }

        final char[][] radarImage = new char[lines.size()][];

        for (int i = 0; i < radarImage.length; i++) {
            radarImage[i] = lines.get(i).toCharArray();
        }

        return radarImage;

    }

}
