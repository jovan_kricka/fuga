package com.fuga.spaceinvaders;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class SpaceInvadersApplication {

    /**
     * Starts spring application. Spring is used only for dependency injection.
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(SpaceInvadersApplication.class, args);
    }

}
