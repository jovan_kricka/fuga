package com.fuga.spaceinvaders.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Model class representing the space invader modeled as matrix of characters.
 */
@Builder
@Getter
@ToString
@EqualsAndHashCode
public class SpaceInvader {

    private final char[][] content;

}
