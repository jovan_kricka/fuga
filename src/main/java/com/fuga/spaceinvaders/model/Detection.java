package com.fuga.spaceinvaders.model;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * Model class representing space invader detection, showing that given space invader
 * was detected at given position.
 */
@Builder
@Getter
@ToString
public class Detection {

    private final int x;
    private final int y;
    private final SpaceInvader spaceInvader;

}
