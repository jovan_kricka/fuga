package com.fuga.spaceinvaders.watcher;

import com.fuga.spaceinvaders.service.LinesProcessor;
import com.fuga.spaceinvaders.service.RadarImageProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.sound.sampled.Line;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;

/**
 * Runnable responsible for watching for new text files that are created in given directory and passing content of those
 * files to the assigned {@link LinesProcessor}.
 */
@Slf4j
@Component
public class FileDropWatcher implements Runnable {

    private final String directoryToWatch;
    private final WatchService watchService;
    private final LinesProcessor linesProcessor;

    /**
     * We assign given file processor that will be used to process detected files, and then we register watch service to
     * listen for new files created in given directory.
     *
     * @param directoryToWatch    string representing path to directory that will be observed
     * @param linesProcessor {@link LinesProcessor} that will handle new files
     * @throws IOException if directory could not be read
     */
    @Autowired
    FileDropWatcher(
            @Value("${app.pathToListen}") final String directoryToWatch,
            final LinesProcessor linesProcessor) throws IOException {

        this.directoryToWatch = directoryToWatch;
        this.linesProcessor = linesProcessor;

        final Path directoryPath = Paths.get(directoryToWatch);
        this.watchService = directoryPath.getFileSystem().newWatchService();
        directoryPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
        log.info("Listening for radar images at ('" + directoryToWatch + "') ...");

    }

    /**
     * Gets new events, representing created files, and submits them to be processed. This work is done until the thread
     * is interrupted.
     */
    @Override
    public void run() {

        try {

            try {

                WatchKey watchKey = null;

                while (!Thread.interrupted()) {

                    watchKey = watchService.take();

                    for (final WatchEvent<?> event : watchKey.pollEvents()) {

                        Thread.sleep(10); // sleep a bit to make sure lock on the file is released by OS
                        handleDetectedEntry(event);

                    }

                    watchKey.reset();
                    log.info("Listening for new changes...");

                }

                if (watchKey != null) {
                    watchKey.cancel();
                }
                watchService.close();

                log.info("Finishing radar image detection service...");

            } catch (InterruptedException e) {
                log.info("Stopping listening for new files on user request.");
            }

        } catch (IOException | InvalidPathException e) {

            log.error("Unable to setup listening on directory changes for directory ('" + directoryToWatch + "').", e);
            log.info("Type 'exit' to leave the program.");

        }

    }

    /**
     * Gets file name from given event, reads its content if it is a text file and passes it to the processor.
     *
     * @param event {@link WatchEvent}
     */
    private void handleDetectedEntry(final WatchEvent<?> event) {

        final String fileName = event.context() == null ? "" : event.context().toString();

        if (StringUtils.isEmpty(fileName) || !fileName.endsWith(".txt")) {

            log.info("Detected new directory entry, but it was not a txt file...");
            return;

        }

        try {

            final List<String> fileContent = Files.readAllLines(Paths.get(fileName));
            linesProcessor.process(fileContent);

        } catch (Exception e) {
            log.error("Reading content of file " + fileName + " failed.", e);
        }

    }

}
